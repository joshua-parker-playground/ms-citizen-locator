FROM maven:3.6.3-jdk-11-openj9

WORKDIR /usr/src/app

EXPOSE 8080

COPY . .

RUN mvn clean install -Dmaven.test.skip=true

CMD ["java", "-jar", "./target/ms-citizen-location-1.0-SNAPSHOT.jar"]