package com.joshparker.citizenlocator.resource;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.testutils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class CitizenResourceImplTest {

    private static final String LONDON   = "London";
    private static final String MOCK_URL = "https://someulr.com";

    @Mock
    private RestTemplate restTemplateMock;

    private CitizenResource sut;


    @BeforeEach
    void beforeEach() {
        this.sut = new CitizenResourceImpl(restTemplateMock, MOCK_URL, MOCK_URL, MOCK_URL);
    }

    @Test
    void getAll_isSuccessful_shouldReturnAllCitizens() throws ApplicationServerError {
        String jsonResponseTestData = TestUtils.getAllCitizensJSON();
        List<Citizen> deserializedTestData = TestUtils.getAllCitizensDeserialized();

        Mockito.when(restTemplateMock.getForEntity(MOCK_URL, String.class))
            .thenReturn(new ResponseEntity(jsonResponseTestData, HttpStatus.OK));

        List<Citizen> response = sut.getAll();

        assertIterableEquals(response, deserializedTestData);
    }

    @Test
    void getByID_isSuccessful_shouldReturnCorrectCitizen() throws ApplicationServerError, CitizenNotFoundException {
        int testID = 3;
        String jsonResponseTestData = TestUtils.getCitizenByIdJson(testID);
        Citizen deserializedTestData = TestUtils.getCitizenByIDDeserialized(testID);

        Mockito.when(restTemplateMock.getForEntity(MOCK_URL + testID, String.class))
                .thenReturn(new ResponseEntity(jsonResponseTestData, HttpStatus.OK));

        Citizen response = sut.getByID(testID);

        assertEquals(response, deserializedTestData);
    }

    @Test
    void getByID_citizenNotFound_shouldThrowCitizenNotFoundException() {
        int testID = 1001;

        Mockito.when(restTemplateMock.getForEntity(MOCK_URL + testID, String.class))
                .thenThrow(new RestClientException("NOT FOUND"));

        assertThrows(CitizenNotFoundException.class, () -> sut.getByID(testID));
    }

    @Test
    void getCitiesCitizens_isSuccessful_shouldReturnAllCitiesCitizens() throws CityNotFoundException, ApplicationServerError {
        String jsonResponseTestData = TestUtils.getAllCitizensJSON();
        List<Citizen> deserializedTestData = TestUtils.getAllCitizensDeserialized();

        Mockito.when(restTemplateMock.getForEntity(MOCK_URL, String.class))
                .thenReturn(new ResponseEntity(jsonResponseTestData, HttpStatus.OK));

        List<Citizen> response = sut.getAllByCity(LONDON);

        assertIterableEquals(response, deserializedTestData);
    }

    @Test
    void getCitiesCitizens_cityNotFound_shouldThrowCityNotFoundException() throws Exception {

        String emptyJsonResponse = TestUtils.getEmptyJsonResponse();

        Mockito.when(restTemplateMock.getForEntity(MOCK_URL, String.class))
                .thenReturn(new ResponseEntity(emptyJsonResponse, HttpStatus.NOT_FOUND));

        assertThrows(CityNotFoundException.class, () -> sut.getAllByCity(LONDON));
    }


}