package com.joshparker.citizenlocator.service;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.resource.CitizenResource;
import com.joshparker.citizenlocator.testutils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class CitizenServiceImplTest {

    private CitizenService sut;

    @Mock
    private CitizenResource citizenResourceMock;

    @BeforeEach
    void beforeEach() {
        this.sut = new CitizenServiceImpl(citizenResourceMock);
    }

    @Test
    void getAll_isSuccessful_shouldReturnAllCitizens() throws ApplicationServerError {
        List<Citizen> allCitizensTestData = TestUtils.getAllCitizensDeserialized();
        when(citizenResourceMock.getAll()).thenReturn(allCitizensTestData);
        List<Citizen> response = sut.getAll();
        assertIterableEquals(response, allCitizensTestData);
    }

    @Test
    void getAll_serverError_shouldThrowApplicationServerError() throws ApplicationServerError {
        when(citizenResourceMock.getAll()).thenThrow(ApplicationServerError.class);
        assertThrows(ApplicationServerError.class, () -> sut.getAll());
    }

    @Test
    void getByID_isSuccessful_shouldReturnCorrectCitizen() throws ApplicationServerError, CitizenNotFoundException {
        int testID = 2;
        Citizen testCitizen = TestUtils.getCitizenByIDDeserialized(testID);
        when(citizenResourceMock.getByID(testID)).thenReturn(testCitizen);
        Citizen response = sut.getByID(testID);
        assertEquals(response, testCitizen);
    }

    @Test
    void getByID_serverError_shouldThrowApplicationServerException() throws ApplicationServerError, CitizenNotFoundException{
        int testID = 1;
        when(citizenResourceMock.getByID(testID)).thenThrow(ApplicationServerError.class);
        assertThrows(ApplicationServerError.class, () -> sut.getByID(testID));
    }

    @Test
    void getByID_citizenNotFound_shouldThrowCitizenNotFotFoundException() throws ApplicationServerError, CitizenNotFoundException{
        int testID = 1;
        when(citizenResourceMock.getByID(testID)).thenThrow(ApplicationServerError.class);
        assertThrows(ApplicationServerError.class, () -> sut.getByID(testID));
    }

    @Test
    void getCitiesCitizens_isSuccessful_shouldReturnAllCitiesCitizens() throws ApplicationServerError, CityNotFoundException {
        List<Citizen> allCitizensTestData = TestUtils.getAllCitizensDeserialized();
        when(citizenResourceMock.getAllByCity(anyString())).thenReturn(allCitizensTestData);
        List<Citizen> response = sut.getAllByCity("London");
        assertIterableEquals(response, allCitizensTestData);
    }

    @Test
    void getCitiesCitizens_serverError_shouldThrowApplicationServerError() throws ApplicationServerError, CityNotFoundException {
        when(citizenResourceMock.getAllByCity(anyString())).thenThrow(ApplicationServerError.class);
        assertThrows(ApplicationServerError.class, () -> sut.getAllByCity("London"));
    }

    @Test
    void getCitiesCitizens_cityNotFound_shouldThrowCityNotFoundException() throws ApplicationServerError, CityNotFoundException {
        when(citizenResourceMock.getAllByCity(anyString())).thenThrow(CityNotFoundException.class);
        assertThrows(CityNotFoundException.class, () -> sut.getAllByCity("London"));
    }
}