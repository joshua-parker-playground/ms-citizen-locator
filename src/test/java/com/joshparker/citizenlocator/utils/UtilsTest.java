package com.joshparker.citizenlocator.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    void capitalizeFirstLetter_wordLengthGreaterThenONE_returnWordWithUppercaseFirstLetter() {
        String input = "test";
        String output = "Test";

        String response = Utils.capitalizeFirstLetter(input);

        assertEquals(response, output);
    }

    @Test
    void capitalizeFirstLetter_wordLengthIsONE_returnsLetterUppercased() {
        String input = "j";
        String output = "J";

        String response = Utils.capitalizeFirstLetter(input);

        assertEquals(response, output);
    }

    @Test
    void capitalizeFirstLetter_emptyString_returnsEmptyString() {
        String input = "";
        String output = "";

        String response = Utils.capitalizeFirstLetter(input);

        assertEquals(response, output);
    }

    @Test
    void capitalizeFirstLetter_nullInput_returnsNull() {
        String input = null;
        String output = null;

        String response = Utils.capitalizeFirstLetter(input);

        assertEquals(response, output);
    }
}