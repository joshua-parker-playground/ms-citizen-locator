package com.joshparker.citizenlocator.testdata;

import com.joshparker.citizenlocator.model.Citizen;

import java.util.Arrays;
import java.util.List;

public class CitizenResourceTestData {
    private CitizenResourceTestData(){}

    public static final List<Citizen> ALL_CITIZENS_DESERIALIZED = Arrays.asList(
            new Citizen(1, "Maurise", "Shieldon", "mshieldon0@squidoo.com", "192.57.232.111", 34.003135, -117.7228641),
            new Citizen(2, "Bendix", "Halgarth", "bhalgarth1@timesonline.co.uk", "4.185.73.82", -2.9623869, 104.7399789),
            new Citizen(3, "Meghan", "Southall", "msouthall2@ihg.com", "21.243.184.215", "15.45033", "44.12768")
    );

    public static final String ALL_CITIZENS_JSON_RESPONSE =
            "[\n" +
                    "  {\n" +
                    "    \"id\": 1,\n" +
                    "    \"first_name\": \"Maurise\",\n" +
                    "    \"last_name\": \"Shieldon\",\n" +
                    "    \"email\": \"mshieldon0@squidoo.com\",\n" +
                    "    \"ip_address\": \"192.57.232.111\",\n" +
                    "    \"latitude\": 34.003135,\n" +
                    "    \"longitude\": -117.7228641\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"id\": 2,\n" +
                    "    \"first_name\": \"Bendix\",\n" +
                    "    \"last_name\": \"Halgarth\",\n" +
                    "    \"email\": \"bhalgarth1@timesonline.co.uk\",\n" +
                    "    \"ip_address\": \"4.185.73.82\",\n" +
                    "    \"latitude\": -2.9623869,\n" +
                    "    \"longitude\": 104.7399789\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"id\": 3,\n" +
                    "    \"first_name\": \"Meghan\",\n" +
                    "    \"last_name\": \"Southall\",\n" +
                    "    \"email\": \"msouthall2@ihg.com\",\n" +
                    "    \"ip_address\": \"21.243.184.215\",\n" +
                    "    \"latitude\": \"15.45033\",\n" +
                    "    \"longitude\": \"44.12768\"\n" +
                    "  }]";
}
