package com.joshparker.citizenlocator.testutils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.joshparker.citizenlocator.model.Citizen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.joshparker.citizenlocator.testdata.CitizenResourceTestData.*;

public class TestUtils {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private TestUtils() {}

    public static final String getAllCitizensJSON() {
        return ALL_CITIZENS_JSON_RESPONSE;
    }

    public static final List<Citizen> getAllCitizensDeserialized() {
        return ALL_CITIZENS_DESERIALIZED;
    }

    public static final String getCitizenByIdJson(int id) {
        String citizenJsonResponse = null;
        try {
            List<Citizen> citizens = Arrays.asList(objectMapper.readValue(ALL_CITIZENS_JSON_RESPONSE, Citizen[].class));
            for (Citizen cit : citizens) {
                if (cit.getId() == id) {
                    citizenJsonResponse = objectMapper.writeValueAsString(cit);
                }
            }
        } catch (IOException e) {

        }
        return citizenJsonResponse;
    }

    public static final Citizen getCitizenByIDDeserialized(int id) {
        Citizen citizen = new Citizen();
        for (Citizen cit : ALL_CITIZENS_DESERIALIZED) {
            if (cit.getId() == id) {
                citizen = cit;
            }
        }
        return citizen;
    }

    public static final String getEmptyJsonResponse() throws IOException {
        List<Citizen> emptyList = new ArrayList<>();
        return objectMapper.writeValueAsString(emptyList);
    }

}
