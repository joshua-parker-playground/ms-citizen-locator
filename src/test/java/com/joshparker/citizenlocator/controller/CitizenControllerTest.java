package com.joshparker.citizenlocator.controller;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.ApplicationExceptionHandler;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.resource.CitizenResource;
import com.joshparker.citizenlocator.service.CitizenService;
import com.joshparker.citizenlocator.testutils.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class CitizenControllerTest {

    private CitizenController sut;

    @Mock
    private CitizenService citizenServiceMock;

    @Mock
    private CitizenResource citizenResourceMock;

    @Mock
    private RestTemplate restTemplateMock;

    private static final String URL = "https://someurl.com";

    private MockMvc mockMvc;

    @BeforeEach
    void beforeEach() {
        this.restTemplateMock = mock(RestTemplate.class);
        this.citizenResourceMock = mock(CitizenResource.class);
        this.citizenServiceMock = mock(CitizenService.class);
        this.sut = new CitizenController(citizenServiceMock);

        mockMvc = MockMvcBuilders.standaloneSetup(sut)
                .setControllerAdvice(new ApplicationExceptionHandler())
                .build();
    }

    @Test
    void getAllCitizens_isSuccessful_shouldReturnSuccessfulResponseEntity() throws ApplicationServerError {
        String allCitizensJSON = TestUtils.getAllCitizensJSON();
        List<Citizen> allCitizensDeserialized = TestUtils.getAllCitizensDeserialized();

        when(restTemplateMock.getForEntity(URL, String.class))
                .thenReturn(new ResponseEntity(allCitizensJSON, HttpStatus.OK));

        when(citizenResourceMock.getAll()).thenReturn(allCitizensDeserialized);
        when(citizenServiceMock.getAll()).thenReturn(allCitizensDeserialized);


        ResponseEntity<List<Citizen>> responseEntity = sut.getAllCitizens();

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), allCitizensDeserialized);
    }

    @Test
    void getAllCitizens_serverError_shouldReturnUnsuccessfulResponseEntity() throws ApplicationServerError, Exception {
        String allCitizensJSON = TestUtils.getAllCitizensJSON();

        when(restTemplateMock.getForEntity(URL, String.class))
                .thenReturn(new ResponseEntity(allCitizensJSON, HttpStatus.OK));

        when(citizenResourceMock.getAll()).thenThrow(ApplicationServerError.class);
        when(citizenServiceMock.getAll()).thenThrow(ApplicationServerError.class);

        // mock mvc has been used because the exception handling is done by spring
        // and during the unit tests, we don't want to boot the spring context
        mockMvc.perform(get("/citizens"))
                .andDo(print())
                .andExpect(status().is5xxServerError());
    }

    @Test
    void getCitizenByID_isSuccessful_shouldReturnSuccessfulResponseEntity() throws ApplicationServerError, CitizenNotFoundException {
        int testID = 1;
        String citizenJson = TestUtils.getCitizenByIdJson(testID);
        Citizen citizenDeserialized = TestUtils.getCitizenByIDDeserialized(testID);

        when(restTemplateMock.getForEntity(URL, String.class))
                .thenReturn(new ResponseEntity(citizenJson, HttpStatus.OK));

        when(citizenResourceMock.getByID(testID)).thenReturn(citizenDeserialized);
        when(citizenServiceMock.getByID(testID)).thenReturn(citizenDeserialized);


        ResponseEntity<Citizen> responseEntity = sut.getCitizenByID(testID);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), citizenDeserialized);
    }

    @Test
    void getCitizenByID_serverError_shouldReturnUnsuccessfulResponseEntity() throws ApplicationServerError,
                                                                                CitizenNotFoundException, Exception {
        int testID = 2;
        String citizenJSON = TestUtils.getCitizenByIdJson(testID);

        when(restTemplateMock.getForEntity(URL, String.class))
                .thenReturn(new ResponseEntity(citizenJSON, HttpStatus.OK));

        when(citizenResourceMock.getByID(testID)).thenThrow(ApplicationServerError.class);
        when(citizenServiceMock.getByID(testID)).thenThrow(ApplicationServerError.class);

        mockMvc.perform(get("/citizens/" + testID))
                .andDo(print())
                .andExpect(status().is5xxServerError());
    }

    @Test
    void getCitizenByID_citizenNotFound_shouldReturnUnsuccessfulResponseEntity() throws ApplicationServerError,
                                                                                        CitizenNotFoundException, Exception {
        int testID = 1001;
        String emptyJsonResponse = TestUtils.getEmptyJsonResponse();

        when(restTemplateMock.getForEntity(URL, String.class))
                .thenReturn(new ResponseEntity(emptyJsonResponse, HttpStatus.NOT_FOUND));

        when(citizenResourceMock.getByID(testID)).thenThrow(CitizenNotFoundException.class);
        when(citizenServiceMock.getByID(testID)).thenThrow(CitizenNotFoundException.class);

        mockMvc.perform(get("/citizens/" + testID))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    void getCitiesCitizens_isSuccessful_shouldReturnSuccessfulResponseEntity() throws ApplicationServerError,
            CityNotFoundException {
        List<Citizen> allCitizensDeserialized = TestUtils.getAllCitizensDeserialized();
        String allCitizensJSON = TestUtils.getAllCitizensJSON();

        when(restTemplateMock.getForEntity("https://someurl.com", String.class))
                .thenReturn(new ResponseEntity(allCitizensJSON, HttpStatus.OK));

        when(citizenResourceMock.getAllByCity(anyString())).thenReturn(allCitizensDeserialized);
        when(citizenServiceMock.getAllByCity(anyString())).thenReturn(allCitizensDeserialized);

        ResponseEntity<List<Citizen>> responseEntity = sut.getAllCitizensByCity("London");

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody(), allCitizensDeserialized);
    }

    @Test
    void getCitiesCitizens_serverError_shouldReturnUnsuccessfulResponseEntity() throws ApplicationServerError,
            CityNotFoundException, Exception {
        String allCitizensJSON = TestUtils.getAllCitizensJSON();

        when(restTemplateMock.getForEntity("https://someurl.com", String.class))
                .thenReturn(new ResponseEntity(allCitizensJSON, HttpStatus.OK));

        when(citizenResourceMock.getAllByCity(anyString())).thenThrow(ApplicationServerError.class);
        when(citizenServiceMock.getAllByCity(anyString())).thenThrow(ApplicationServerError.class);

        // mock mvc has been used because the exception handling is done by spring
        // and during the unit tests, we don't want to boot the spring context
        mockMvc.perform(get("/citizens?city=london"))
                .andDo(print())
                .andExpect(status().is5xxServerError());
    }

    @Test
    void getCitiesCitizens_cityNotFound_shouldReturnUnsuccessfulResponseEntity() throws ApplicationServerError,
            CityNotFoundException, Exception {
        String emptyJsonResponse = TestUtils.getEmptyJsonResponse();

        when(restTemplateMock.getForEntity("https://someurl.com", String.class))
                .thenReturn(new ResponseEntity(emptyJsonResponse, HttpStatus.NOT_FOUND));

        when(citizenResourceMock.getAllByCity(anyString())).thenThrow(CityNotFoundException.class);
        when(citizenServiceMock.getAllByCity(anyString())).thenThrow(CityNotFoundException.class);

        // mock mvc has been used because the exception handling is done by spring
        // and during the unit tests, we don't want to boot the spring context
        mockMvc.perform(get("/citizens?city=london"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }
}