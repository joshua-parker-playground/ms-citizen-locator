package com.joshparker.citizenlocator;

import com.joshparker.citizenlocator.controller.CitizenController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitizenLocator {
    private static final Logger LOGGER = LoggerFactory.getLogger(CitizenController.class);

    public static void main(String[] args) {
        SpringApplication.run(CitizenLocator.class, args);
        LOGGER.info("Citizen Location application started on port 8080");
    }
}
