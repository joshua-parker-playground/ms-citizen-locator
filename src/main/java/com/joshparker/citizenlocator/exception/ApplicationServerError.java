package com.joshparker.citizenlocator.exception;

public class ApplicationServerError extends Throwable {

    public ApplicationServerError(String errorDescription) {
        super(errorDescription);
    }
}
