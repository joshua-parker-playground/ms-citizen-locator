package com.joshparker.citizenlocator.exception;

public class ResponseInfo {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
