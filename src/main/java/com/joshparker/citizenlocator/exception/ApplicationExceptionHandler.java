package com.joshparker.citizenlocator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(ApplicationServerError.class)
    public ResponseEntity<ResponseInfo> applicationServerError(ApplicationServerError exception) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setMessage(exception.getMessage());

        return new ResponseEntity<>(responseInfo, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(CitizenNotFoundException.class)
    public ResponseEntity<ResponseInfo> cityLocationException(CitizenNotFoundException exception) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setMessage(exception.getMessage());

        return new ResponseEntity<>(responseInfo, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CityNotFoundException.class)
    public ResponseEntity<ResponseInfo> cityLocationException(CityNotFoundException exception) {
        ResponseInfo responseInfo = new ResponseInfo();
        responseInfo.setMessage(exception.getMessage());

        return new ResponseEntity<>(responseInfo, HttpStatus.NOT_FOUND);
    }
}
