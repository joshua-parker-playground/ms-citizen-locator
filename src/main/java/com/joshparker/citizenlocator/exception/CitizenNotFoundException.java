package com.joshparker.citizenlocator.exception;

public class CitizenNotFoundException extends Throwable {

    public CitizenNotFoundException(String errorDescription) {
        super(errorDescription);
    }

}
