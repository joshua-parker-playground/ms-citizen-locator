package com.joshparker.citizenlocator.exception;

public class CityNotFoundException extends Throwable {

    public CityNotFoundException(String errorDescription) {
        super(errorDescription);
    }
}
