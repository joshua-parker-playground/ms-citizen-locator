package com.joshparker.citizenlocator.constants;

public class CitizenLocatorConstants {
    private CitizenLocatorConstants() {}

    public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error. Try again later.";
    public static final String CITIZEN_NOT_FOUND_MESSAGE = "Citizen was not found";
    public static final String CITY_NOT_FOUND_MESSAGE = "No citizens were found for you specified city.";
}
