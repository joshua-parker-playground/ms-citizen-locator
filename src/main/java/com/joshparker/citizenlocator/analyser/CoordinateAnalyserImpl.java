package com.joshparker.citizenlocator.analyser;

import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CoordinateAnalyserImpl implements CoordinateAnalyser {
    private static final Logger LOGGER = LoggerFactory.getLogger(CoordinateAnalyserImpl.class);
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

    public List<Citizen> getAllCitizensWithinRadiusOfLocation(List<Citizen> citizens, Location location, double radius) {
        LOGGER.info("Getting any citizens outside of city within a radius of {}", radius);
        return getCitizensWithinRadius(citizens, radius, location);
    }

    private List<Citizen> getCitizensWithinRadius(List<Citizen> citizens, double radius, Location location) {
        double locationLatitude = Math.toRadians(location.getLatitude());
        double locationLongitude = Math.toRadians(location.getLongitude());
        return filterCitizensWithinRadius(citizens, radius, locationLatitude, locationLongitude);
    }

    private List<Citizen> filterCitizensWithinRadius(List<Citizen> citizens, double radius, double locationLatitude, double locationLongitude) {
        List<Citizen> citizensWithinRadius = new ArrayList<>();
        for (Citizen citizen : citizens) {
            double citizenLatitude = Math.toRadians(citizen.getLatitude());
            double citizenLongitude = Math.toRadians(citizen.getLongitude());
            double angle = getAngle(locationLatitude, locationLongitude, citizenLatitude, citizenLongitude);
            double nauticalMiles = 60 * Math.toDegrees(angle);
            double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;

            if (statuteMiles <= radius) {
                citizensWithinRadius.add(citizen);
            }
        }
        return citizensWithinRadius;
    }

    private double getAngle(double locationLatitude, double locationLongitude, double citizenLatitude, double citizenLongitude) {
        return Math.acos(Math.sin(locationLatitude) * Math.sin(citizenLatitude)
                        + Math.cos(locationLatitude) * Math.cos(citizenLatitude) * Math.cos(locationLongitude - citizenLongitude));
    }
}
