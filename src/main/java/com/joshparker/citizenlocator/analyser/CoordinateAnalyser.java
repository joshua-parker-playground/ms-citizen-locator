package com.joshparker.citizenlocator.analyser;

import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.model.Location;

import java.util.List;

public interface CoordinateAnalyser {
    List<Citizen> getAllCitizensWithinRadiusOfLocation(List<Citizen> citizens, Location location, double radius);
}
