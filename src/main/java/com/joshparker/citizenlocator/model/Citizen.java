package com.joshparker.citizenlocator.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Citizen<T> {
    private int id;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String email;
    @JsonProperty("ip_address")
    private String ipAddress;
    private double latitude;
    private double longitude;

    public Citizen() {}

    public Citizen(int id, String firstName, String lastName, String email, String ipAddress, T latitude, T longitude) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.ipAddress = ipAddress;
        this.latitude = convertToDouble(latitude);
        this.longitude = convertToDouble(longitude);
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private double convertToDouble(T input) {
        if (input instanceof String) {
            Double dObj = new Double(input.toString());
            return dObj.doubleValue();
        }
        double newInput = (double) input;
        return newInput;
    }

    @Override
    public String toString() {
        return "Citizen{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Citizen citizen = (Citizen) o;
        return id == citizen.id &&
                Double.compare(citizen.latitude, latitude) == 0 &&
                Double.compare(citizen.longitude, longitude) == 0 &&
                Objects.equals(firstName, citizen.firstName) &&
                Objects.equals(lastName, citizen.lastName) &&
                Objects.equals(email, citizen.email) &&
                Objects.equals(ipAddress, citizen.ipAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, ipAddress, latitude, longitude);
    }
}
