package com.joshparker.citizenlocator.controller;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.service.CitizenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("CitizenController")
public class CitizenController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CitizenController.class);

    @Autowired
    private CitizenService citizenService;

    public CitizenController() {}

    public CitizenController(CitizenService citizenService) {
        this.citizenService = citizenService;
    }

    /**
     * Returns all citizens
     *
     * @return ResponseEntity
     * @throws ApplicationServerError
     */
    @GetMapping("/citizens")
    public ResponseEntity<List<Citizen>> getAllCitizens() throws ApplicationServerError {
        LOGGER.info("Getting all citizens: endpoint: /citizens");

        List<Citizen> citizens = citizenService.getAll();
        return new ResponseEntity<>(citizens, HttpStatus.OK);
    }

    /**
     * Returns specified citizen
     *
     * @param id
     * @return ResponseEntity
     * @throws ApplicationServerError
     * @throws CitizenNotFoundException
     */
    @GetMapping("/citizens/{id}")
    public ResponseEntity<Citizen> getCitizenByID(@PathVariable int id) throws ApplicationServerError, CitizenNotFoundException {
        LOGGER.info("Entered Citizen Controller. Endpoint: /citizens/{}", id);

        Citizen citizen = citizenService.getByID(id);
        return new ResponseEntity<>(citizen, HttpStatus.OK);
    }

    /**
     * Returns all citizens in a specified city
     *
     * @param city
     * @return ResponseEntity
     * @throws ApplicationServerError
     * @throws CityNotFoundException
     */
    @RequestMapping(value = "/citizens", method = RequestMethod.GET, params = "city")
    public ResponseEntity<List<Citizen>> getAllCitizensByCity(@RequestParam String city)
            throws ApplicationServerError, CityNotFoundException {
        LOGGER.info("Getting all citizens in {}: Endpoint: /citizens?city={}", city, city);

        List<Citizen> citizens = citizenService.getAllByCity(city);
        return new ResponseEntity<>(citizens, HttpStatus.OK);
    }

    /**
     * Returns all citizens in and within a specified radius in a specified city
     *
     * @param city
     * @param radius
     * @return ResponseEntity
     * @throws ApplicationServerError
     * @throws CityNotFoundException
     */
    @RequestMapping(value = "/citizens", method = RequestMethod.GET, params = {"city", "radius"})
    public ResponseEntity<List<Citizen>> getAllCitizensByCityWithRadius(@RequestParam String city, @RequestParam double radius)
            throws ApplicationServerError, CityNotFoundException {
        LOGGER.info("Getting all citizens in and within {}, with a radius of {} miles: " +
                "Endpoint: /citizens?city={}&radius={}", city, radius, city, radius);

        List<Citizen> citizensInAndWithinRadiusOfCity = citizenService.getAllByCityWithRadius(city, radius);
        return new ResponseEntity<>(citizensInAndWithinRadiusOfCity, HttpStatus.OK);
    }
}
