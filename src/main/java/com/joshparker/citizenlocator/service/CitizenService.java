package com.joshparker.citizenlocator.service;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CitizenService")
public interface CitizenService {

    /**
     * Returns all citizens
     *
     * @return List<Citizen>
     * @throws ApplicationServerError
     */
    List<Citizen> getAll() throws ApplicationServerError;

    /**
     * Returns specified citizen
     *
     * @param id
     * @return Citizen
     * @throws ApplicationServerError
     * @throws CitizenNotFoundException
     */
    Citizen getByID(int id) throws ApplicationServerError, CitizenNotFoundException;

    /**
     * Returns all citizens in a specified city
     *
     * @param city
     * @return List<Citizen>
     * @throws ApplicationServerError
     * @throws CityNotFoundException
     */
    List<Citizen> getAllByCity(String city) throws ApplicationServerError, CityNotFoundException;

    /**
     * Returns all citizens in and within a specified radius of a specified city
     *
     * @param city
     * @param radius
     * @return List<Citizen>
     * @throws ApplicationServerError
     * @throws CityNotFoundException
     */
    List<Citizen> getAllByCityWithRadius(String city, double radius) throws ApplicationServerError, CityNotFoundException;
}
