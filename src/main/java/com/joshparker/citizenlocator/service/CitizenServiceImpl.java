package com.joshparker.citizenlocator.service;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.analyser.CoordinateAnalyserImpl;
import com.joshparker.citizenlocator.analyser.CoordinateAnalyser;
import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.model.Location;
import com.joshparker.citizenlocator.resource.CitizenResource;
import com.joshparker.citizenlocator.resource.LocationResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("CitizenServiceImpl")
public class CitizenServiceImpl implements CitizenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CitizenServiceImpl.class);

    @Autowired
    private CitizenResource citizenResource;

    @Autowired
    private LocationResource locationResource;

    public CitizenServiceImpl() {}

    public CitizenServiceImpl(CitizenResource citizenResource) {
        this.citizenResource = citizenResource;
    }

    public List<Citizen> getAll() throws ApplicationServerError {
        LOGGER.info("Getting all citizens");
        return citizenResource.getAll();
    }

    public Citizen getByID(int id) throws ApplicationServerError, CitizenNotFoundException {
        LOGGER.info("Entered Citizen Service. Getting citizen with ID: {}", id);
        return citizenResource.getByID(id);
    }

    public List<Citizen> getAllByCity(String city) throws ApplicationServerError, CityNotFoundException {
        LOGGER.info("Getting all citizens in {}", city);
        return citizenResource.getAllByCity(city);
    }

    public List<Citizen> getAllByCityWithRadius(String city, double radius) throws ApplicationServerError, CityNotFoundException {
        LOGGER.info("Getting all citizens in and within {}, with a radius of {} miles", city, radius);

        List<Citizen> citizensInCity = citizenResource.getAllByCity(city);
        List<Citizen> allCitizensOutsideOfCity = getAllCitizensOutsideOfCity(citizensInCity);

        Location cityCoordinates = locationResource.getCoordinates(city);

        CoordinateAnalyser citizenCoordinateAnalyser = new CoordinateAnalyserImpl();
        List<Citizen> citizensOutsideOfCityWithinRadius =
                citizenCoordinateAnalyser.getAllCitizensWithinRadiusOfLocation(allCitizensOutsideOfCity, cityCoordinates, radius);

        return citizensInAndAroundSpecifiedRadius(citizensInCity, citizensOutsideOfCityWithinRadius);
    }

    private List<Citizen> getAllCitizensOutsideOfCity(List<Citizen> citizensInCity) throws ApplicationServerError {
        return citizenResource
                .getAll()
                .stream()
                .filter(i -> !citizensInCity.contains(i))
                .collect(Collectors.toList());
    }

    private List<Citizen> citizensInAndAroundSpecifiedRadius(List<Citizen> citizensInCity, List<Citizen> citizensOutsideOfCity) {
        return Stream.concat(citizensInCity.stream(), citizensOutsideOfCity.stream())
                .collect(Collectors.toList());
    }
}
