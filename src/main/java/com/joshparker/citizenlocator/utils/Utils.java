package com.joshparker.citizenlocator.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Utils {
    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    private Utils() {}

    public static String capitalizeFirstLetter(String word) {
        LOGGER.info("Capitalizing first letter in word: {}", word);
        if (word == null) {
            return null;
        } else if (word.length() < 2) {
            return word.toUpperCase();
        }
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }

    public static String constructURL(String city, String URL) {
        String preFixURL = URL;
        if (Character.isLowerCase(city.charAt(0))) {
            return preFixURL.replace("{city}", Utils.capitalizeFirstLetter(city));
        }
        return preFixURL.replace("{city}", city);
    }
}