package com.joshparker.citizenlocator.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.joshparker.citizenlocator.constants.CitizenLocatorConstants.CITY_NOT_FOUND_MESSAGE;
import static com.joshparker.citizenlocator.constants.CitizenLocatorConstants.INTERNAL_SERVER_ERROR_MESSAGE;
import static com.joshparker.citizenlocator.utils.Utils.constructURL;

@Component("LocationResourceImpl")
public class LocationResourceImpl implements LocationResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationResourceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    @Value("${url.locationCoordinates}")
    private String locationCoordinatesUrl;

    public LocationResourceImpl() {}

    public LocationResourceImpl(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.mapper = objectMapper;
    }

    public Location getCoordinates(String city) throws ApplicationServerError, CityNotFoundException {
        LOGGER.info("Getting latitude and longitude for specified city");
        Location location;
        Gson gson;

        try {
            ResponseEntity<Map> response = restTemplate.getForEntity(constructURL(city, locationCoordinatesUrl), Map.class);
            gson = new Gson();
            Map responseMap = gson.fromJson(mapper.writeValueAsString(response.getBody()), Map.class);
            double lat = Double.parseDouble((String) responseMap.get("latt"));
            double lon = Double.parseDouble((String) responseMap.get("longt"));
            location = new Location(lat, lon);

            LOGGER.info("Coordinates found: latitude: {} longitude: {}", lat, lon);
        } catch (JsonProcessingException e) {
            LOGGER.info("Unable to parse json: {}", e);
            throw new ApplicationServerError(INTERNAL_SERVER_ERROR_MESSAGE);
        } catch (RestClientException e) {
            LOGGER.info("City was coordinates were not found: {}", e);
            throw new CityNotFoundException(CITY_NOT_FOUND_MESSAGE);
        }
        return location;
    }
}