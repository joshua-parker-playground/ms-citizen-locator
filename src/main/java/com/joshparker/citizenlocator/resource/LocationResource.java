package com.joshparker.citizenlocator.resource;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Location;
import org.springframework.stereotype.Component;

@Component("LocationResource")
public interface LocationResource {

    /**
     * Returns geocode information such as latitude and longitude of a specified city
     *
     * @param city
     * @return
     * @throws ApplicationServerError
     * @throws CityNotFoundException
     */
    Location getCoordinates(String city) throws ApplicationServerError, CityNotFoundException;
}
