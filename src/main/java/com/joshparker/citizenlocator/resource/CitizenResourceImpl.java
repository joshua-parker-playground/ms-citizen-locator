package com.joshparker.citizenlocator.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import com.joshparker.citizenlocator.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.joshparker.citizenlocator.constants.CitizenLocatorConstants.*;
import static com.joshparker.citizenlocator.utils.Utils.constructURL;

@Service("CitizenResourceImpl")
public class CitizenResourceImpl implements CitizenResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(CitizenResourceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper mapper;

    @Value("${url.citizens}")
    private String allCitizensURL;

    @Value("${url.citizensById}")
    private String citizensByIdPrefix;

    @Value("${url.citiesCitizens}")
    private String citizensByIdUrl;

    public CitizenResourceImpl() {}

    public CitizenResourceImpl(RestTemplate restTemplate, ObjectMapper mapper) {
        this.restTemplate = restTemplate;
        this.mapper = mapper;
    }

    public CitizenResourceImpl(RestTemplate restTemplate, String allCitizensURL,
                               String citizensByIdPrefix, String citizensByIdUrl) {
        this.restTemplate = restTemplate;
        this.allCitizensURL = allCitizensURL;
        this.citizensByIdPrefix = citizensByIdPrefix;
        this.citizensByIdUrl = citizensByIdUrl;
        this.mapper = new ObjectMapper();
    }

    public List<Citizen> getAll() throws ApplicationServerError {
        LOGGER.info("Attempting to retrieve all citizens with URL: {}", allCitizensURL);
        List<Citizen> citizens;

        try {
            ResponseEntity<String> response = restTemplate.getForEntity(allCitizensURL, String.class);
            citizens = Arrays.asList(mapper.readValue(response.getBody(), Citizen[].class));
        } catch (IOException  e) {
            LOGGER.error("IOException was caught: {}", e);
            throw new ApplicationServerError(INTERNAL_SERVER_ERROR_MESSAGE);
        }

        LOGGER.info("All citizens were found");
        return citizens;
    }

    public Citizen getByID(int id) throws ApplicationServerError, CitizenNotFoundException {
        LOGGER.info("Attempting to retrieve citizens: ID {}, URL: {}", id, citizensByIdPrefix);
        Citizen citizen;

        try {
            ResponseEntity<String> response = restTemplate.getForEntity(citizensByIdPrefix + id, String.class);
            citizen = mapper.readValue(response.getBody(), Citizen.class);

        } catch (IOException e) {
            throw new ApplicationServerError(INTERNAL_SERVER_ERROR_MESSAGE);
        } catch (RestClientException e) {
            LOGGER.error("RestClientException was caught: {}", e);
            throw new CitizenNotFoundException(CITIZEN_NOT_FOUND_MESSAGE);
        }

        LOGGER.info("Citizen was found with ID: {}", id);
        return citizen;
    }

    public List<Citizen> getAllByCity(String city) throws ApplicationServerError, CityNotFoundException {
        LOGGER.info("Attempting to retrieve all cities citizens with URL: {}", citizensByIdUrl);
        List<Citizen> citizens;

        try {
            ResponseEntity<String> response = restTemplate.getForEntity(constructURL(city, citizensByIdUrl), String.class);
            citizens = Arrays.asList(mapper.readValue(response.getBody(), Citizen[].class));

            if (citizens.isEmpty()) {
                LOGGER.info("City was not found");
                throw new CityNotFoundException(CITY_NOT_FOUND_MESSAGE);
            }

        } catch (IOException e) {
            LOGGER.error("IOException was caught: {}", e);
            throw new ApplicationServerError(INTERNAL_SERVER_ERROR_MESSAGE);
        }

        LOGGER.info("All cities citizens were found");
        return citizens;
    }
}
