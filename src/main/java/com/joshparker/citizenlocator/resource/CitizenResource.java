package com.joshparker.citizenlocator.resource;

import com.joshparker.citizenlocator.exception.ApplicationServerError;
import com.joshparker.citizenlocator.exception.CitizenNotFoundException;
import com.joshparker.citizenlocator.exception.CityNotFoundException;
import com.joshparker.citizenlocator.model.Citizen;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("CitizenResource")
public interface CitizenResource {

    /**
     * Returns all citizens
     *
     * @return List<Citizen>
     * @throws ApplicationServerError
     */
    List<Citizen> getAll() throws ApplicationServerError;

    /**
     * Returns a specified citizen
     *
     * @param id
     * @return Citizen
     * @throws ApplicationServerError
     * @throws CitizenNotFoundException
     */
    Citizen getByID(int id) throws ApplicationServerError, CitizenNotFoundException;

    /**
     * Returns all citizens from specified city
     *
     * @return List<Citizen>
     * @throws ApplicationServerError
     * @throws CityNotFoundException
     */
    List<Citizen> getAllByCity(String city) throws ApplicationServerError, CityNotFoundException;
}
