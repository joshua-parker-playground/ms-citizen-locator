# Project Title

ms-citizen-locator

### Prerequisites

You will need to have maven and Java 11 installed on your machine to run this app.

Alternatively, if you have docker installed, you only need to run "docker-compose up" and then navigate to one of the endpoints listed below.

## Getting Started
If your using docker, simply run

```
docker-compose up -d
```

If your using IntelliJ, you can simply open the project up, build and run the project within intelliJ.


If your running the application from the command line, the first thing you will want to do is install project dependencies and package the project. In order to do this, navigate to the root of the project and run the command

```
mvn clean install
```

This command executes multiple maven tasks and are as follows:

- creates a target folder (mvn clean)
- compiles the application (mvn compile)
- runs the tests (mvn test)
- packages the project into an executable JAR file (mvn package)
- adds project to your local maven repository (mvn install)

Once the build has finished, run the following
```
java -jar ./target/ms-citizen-location-1.0-SNAPSHOT.jar
```

## Running the tests

You can run the tests in intellij, or if you want to run the test from the command line, navigate to the root of the project and run the command

```
mvn test
```

## Endpoints

This application has 4 endpoints

```
http://localhost:8080/citizens?city=<city>&radius=<miles>
```
This will return all/any citizens in and within a specified radius of a specified city

```
http://localhost:8080/citiznes?city=<city>
```
This will return all citizens in a specified city

```
http://localhost:8080/citizens
```
This will return all citizens

```
http://localhost:8080/citizens/<id>
```
This will return a specified citizen
